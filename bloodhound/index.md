---
Title: bloodhound
Homepage: https://github.com/BloodHoundAD/BloodHound
Repository: https://gitlab.com/kalilinux/packages/bloodhound
Architectures: amd64 arm64
Version: 4.1.0-0kali1
Metapackages: kali-linux-everything 
Icon: images/bloodhound-logo.svg
PackagesInfo: |
 ### bloodhound
 
  This package contains BloodHound, a single page Javascript web application.
  BloodHound uses graph theory to reveal the hidden and often unintended
  relationships within an Active Directory environment. Attackers can use
  BloodHound to easily identify highly complex attack paths that would otherwise
  be impossible to quickly identify. Defenders can use BloodHound to identify and
  eliminate those same attack paths. Both blue and red teams can use BloodHound
  to easily gain a deeper understanding of privilege relationships in an Active
  Directory environment.
 
 **Installed size:** `262.59 MB`  
 **How to install:** `sudo apt install bloodhound`  
 
 {{< spoiler "Dependencies:" >}}
 * libasound2 
 * libatk-bridge2.0-0 
 * libatk1.0-0 
 * libatspi2.0-0 
 * libc6 
 * libcairo2 
 * libcups2 
 * libdbus-1-3 
 * libdrm2 
 * libexpat1 
 * libgbm1 
 * libgcc-s1 
 * libgdk-pixbuf-2.0-0 
 * libglib2.0-0 
 * libgtk-3-0 
 * libnspr4 
 * libnss3 
 * libpango-1.0-0 
 * libpangocairo-1.0-0 
 * libx11-6 
 * libx11-xcb1 
 * libxcb1 
 * libxcomposite1 
 * libxdamage1 
 * libxext6
 * libxfixes3
 * libxkbcommon0 
 * libxrandr2
 * neo4j
 {{< /spoiler >}}
 
 ##### bloodhound
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
